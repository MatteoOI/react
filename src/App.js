import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
//importiamo il componente hellow creato da noi e specifichiamo da dove importarlo
import Hellow from './hellow';
import Hellow2 from './hellow2';

import Clock from './Clock';
import Clock2 from './Clock2';
import Timer from './Timer';

const clocks = [
  {
    secs:1,
    country:'Italy',
    timezone:0
  },
  {
    secs:3,
    country:'Turkey',
    timezone:2
  },
  {
    secs:4,
    country:'Cuba',
    timezone:-6
  },
]
class App extends Component {
  //quando si crea un componente dobbiamo mettere "class Apps extends Component"
  //importante che in alto il component sia definito nell'importante
  //inseriamo il metodo render e riportiamo lo scheletro all'interno del render
  getClocks() {
    return clocks.map( ({secs, country, timezone}) => {
      // prende gli elementi nell'array e genera per ognuno un clock
      // con i valori dell'array. React ha bisogno di un valore key univoco
      // per ogni oggetto. Per questo si inserisce in questo caso il campo
      // county che sarà differente per ogni oggetto
      return <Clock
        key={country}
        secs={secs}
        country={country}
        timezone={timezone} />
      })
      }
  render() { //viene invocato quando montiamo un nostro componente di tipo Class
    // gle lementi devono avere un elemento radice, le espressioni
    //JSX devono contenere un solo elemento padre. Se non vogliamo aggiungere
    // un elemento padre intorno possiamo utilizzare <React.Fragment></..> o <></>
    const happy = <h2>I am happy</h2>;
    // function getDate(date) {
    //    return date.toLocaleDateString()  + ' ' + date.toLocaleTimeString()
    // }
    // PER QUESTA FUNZIONE inserire nel return   <h2> Today is { getDate(new Date()) } </h2>

    return (
      <>

      <h2>My first React App</h2>
      <ul>
      {this.getClocks()}
      </ul>
      <Timer secs="1" timezone="0" />

      </>
    );
  }
}

export default App;
