import React from 'react'

class Clock extends React.Component {


  // con la classe si estende il componente creato in react
  constructor(props) {
    //nel caso in cui sia una classe e si debba voglia prendere in considerazione
    // le props dobbiamo utilizzare un costruttore, ma siccome le props sono al
    // livello superiore dobbiamo esternderle dal genitore tramite il comando super()
    super(props);
    // inizializziamo lo stato. è il modo in cui indichiamo a react che vogliamo aggiornare
    // lo stato e possiamo inserire ogni proprietà che vogliamo qua dentro

    // this.state = {
    //   date: new Date(),
    //   timezone: 1,
    //   country:'italy'
    // }

    this.state = {
      timestamp: Date.now(),
      date: new Date(),
      timezone: 1,
      country:'italy',
      stopped:false,

    }
  }

  render() {

    const d = new Date(this.state.timestamp);
    const tempo = d.getTime() + this.props.timezone*3600*1000;
    // const tempo = this.state.date.getTime() + this.props.timezone*3600*1000;

    /// se stampo le props console.log(props) vedrò i timezone di ogni clocks
    /// definiti dentro app.js alla stringa <Clock/>
    // datp che devo aggiungere ad esempio 2 ore al mio orologio faccio
    // props.timezone (2) * 3600 (secondi in un'ora), * 1000 (il tempo è espresso in millisecondi)
    const data = new Date(tempo);

    return <li>
    In { this.props.country} is <span className="clock">{ data.toLocaleTimeString() }</span>
    <button onClick={this.toggleWatch}>{this.state.stopped ? 'start' : 'stop'}</button>
     </li>
  }

  // Per cambiare lo stato andiamo a inserire un setInterval che ogni x secondi richiama il render()
  // chiaramente non possiamo aggiungere nel metodo render() il setInterval perchè si creerebbe un loop
  // infinito. Il setInterval va chiamato fuori in una funzione.
  toggleWatch = (e) => {
    this.setState( (state,props) => {
      state.stopped ? this.startWatch() : clearInterval(this.interval);
      return {stopped: !state.stopped};
    })
  }

  tick = () => {
  //  this.setState({
      // this.setState va a modificare tutti gli stati presenti in state. Modifica solo quelli che
      // incontra in entrambe le definizioni, lasciando intatti gli altri stati dichiarati.
      // in questo caso solo date e non timezone.

      //date: new Date(),
    //});
    this.setState ( (precState,props) => {
      return {
        timestamp: precState.timestamp + props.secs*1000
      }
    });
  };

  startWatch() {
    // this.interval è l'id del nostro loop creato dal setInterval, serve definirlo perchè quando il componente
    // non esisterà più dovremmo andare a pulire l'app da questo loop che consuma memoria. La pulizia avviene
    // tramite il metodo clear (vedi sotto).
    this.interval = setInterval(this.tick , this.props.secs *1000 );
  }

  componentDidMount() {
    // componentDidMount modifica l'oggetto una volta che il componente viene montato, trovi altri stati sulla
    // documentazione di react. Sotto stiamo chiamando con il setInterval la funzione tick ogni secondo
    console.log('component did update');
    this.startWatch();
  }
  // componentWillUnmount serve a far capire quando il componente viene smontato
  componentWillUnmount() {
    clearInterval(this.interval);
  }
}

export default Clock;
