import React from 'react';

class Clock2 extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const tempo = Date.now() + this.props.timezone * 3600 * 1000;

    const data = new Date(tempo);
    return <h2> in {this.props.country} is {data.toLocaleDateString() + ' ' + data.toLocaleTimeString()} </h2>

  }
}

export default Clock2;


//import React from 'react'

//function clock(props){
  //const tempo = Date.now() + props.timezone*3600*1000;
    /// se stampo le props console.log(props) vedrò i timezone di ogni clocks
    /// definiti dentro app.js alla stringa <Clock/>
    // datp che devo aggiungere ad esempio 2 ore al mio orologio faccio
    // props.timezone (2) * 3600 (secondi in un'ora), * 1000 (il tempo è espresso in millisecondi)
  //const data = new Date(tempo);
  //return <h2>In {props.country} Today is { data.toLocaleDateString() + ' ' + data.toLocaleTimeString() } </h2>
//}

//export default clock;
