import React from 'react'

class Timer extends React.Component {
  constructor(props) {

    super(props)
    this.toggleWatch = this.toggleWatch.bind(this);
    this.resetTimer = this.resetTimer.bind(this)
    let d = new Date();
    let tempo = d.setHours(0,0,0,0);
    this.state = {
      timestamp:tempo,
      date: new Date(),
      timezone: 1,
      country:'italy',
      stopped:true,
    }
  }

  render() {

    let d = new Date(this.state.timestamp);
    let tempo = d.getTime() + this.props.timezone*3600*1000;
    let data = new Date(tempo);

    return <h2>{ data.toLocaleTimeString() }
    <button onClick={this.toggleWatch}>{this.state.stopped ? 'start' : 'stop'}</button>
    <button onClick={this.resetTimer}>Reset</button>
     </h2>
  }

  resetTimer(e) {
    this.setState( (state, props) => {

      state.stopped ? clearInterval(this.interval) : clearInterval(this.interval)
      let d = new Date();
      let tempo = d.setHours(0,0,0,0);
      let data = new Date(tempo);
      return {
        timestamp: tempo,
        stopped:true
      }

    })
  }

  toggleWatch(e) {
    this.setState( (state , props) => {
      state.stopped ? this.startWatch() : clearInterval(this.interval)
      return {stopped: ! state.stopped}
    });
  }

  tick = () => {
    this.setState ( (precState,props) => {
      return {
        timestamp: precState.timestamp + props.secs*1000
      }
    })
  }

  startWatch() {
    this.interval = setInterval(this.tick , this.props.secs *1000 );
  }

  componentDidMount() {
    //this.startWatch();
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }
}

export default Timer;
