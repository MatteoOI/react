import React from 'react';

class Hellow2 extends React.Component {
  constructor(props) {
    super(props);
    console.log(props);
  }
  render() {
    let props = this.props;
    return <h2>{ props.one }</h2>
  }
}

export default Hellow2
