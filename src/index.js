import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

//RENDERIZZIAMO CONTINUAMENTE CIò CHE CAMBIA
function renderApp() {
  ReactDOM.render(<App />, document.getElementById('root'));

}
//ognu secondo react va a renderizzare la app e a cambiare solo le parti
//che cambieranno lasciando invariate le altre parti.
setInterval(renderApp, 1000)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
